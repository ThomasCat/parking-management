# Parking Management

A really simple Java program I wrote to remember parked vehicles, for my college's Java Programming Lab course (15CS46P, [DTE Karnataka](http://dte.kar.nic.in/indexe.shtml?en)).

<b>License</b>:<br>
<a href="https://www.apache.org/licenses/LICENSE-2.0" rel="nofollow"><img src="https://www.apache.org/img/ASF20thAnniversary.jpg" alt="GNU GPLv3 Image" data-canonical-src="https://www.apache.org/img/ASF20thAnniversary.jpg" width="80"></a><br>[Copyright © Owais Shaikh 2017](LICENSE)