/****************
Copyright 2017 Owais Shaikh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*******************/
//This code was written on Wednesday, 12th March, 2017 
//and last edited on Monday, 20th March, 2017

import java.io.IOException;
import java.io.DataInputStream;
import java.lang.*;

class vehicle{
	parkingManagement mainClass=new parkingManagement();
	DataInputStream userInput=new DataInputStream(System.in);
	int cap=20;
	String vehicleType;//[]=new String[cap]; 
	int licensePlateNo;//[]=new int[cap];
	
	public void getData(){
		try{
			for(int i=0; i<=cap; i++){
				System.out.print("\nEnter your vehicle type: ");
				vehicleType=userInput.readLine();
				System.out.print("Enter your " +vehicleType +"'s license plate number: ");
				licensePlateNo=Integer.parseInt(userInput.readLine());
				System.out.print("\n");
				System.out.print("Enter another vehicle?(Press '1' for YES or '0' for NO): ");
				int next=Integer.parseInt(userInput.readLine());
				if(next==1){
					
				}else{
					String[] args={};
					mainClass.main(args);
					break;
				}
			}
		}catch(Exception e){}
	}
	
	public void display(){
		for(int i=0; i<=cap; i++){
			System.out.println("  " +vehicleType+"\t\t\t"+licensePlateNo);
		}
	}
	
	public void removeData(){
		System.out.println("***********************************");
  	System.out.println("*Your total fare is: $50          *");
    System.out.println("***********************************\n\n");
    vehicleType="NO VEHICLES PARKED";
    licensePlateNo=000000000;
	}
	
}

class userInterface{
		
		static void greet(){
	  	System.out.println("***********************************");
      System.out.println("*    PARKING MANAGEMENT SYSTEM    * \n*         by Group #5             *");
      System.out.println("***********************************");
      System.out.println("*    Press '1' to park vehicle    *");
      System.out.println("*    Press '2' to exit vehicle    *");
      System.out.println("*    Press '3' to view all        *");
      System.out.println("*    Press '4' to exit            *");
      System.out.println("***********************************");
      System.out.print("* ENTER YOUR CHOICE: ");
		}
		
		static void table(){
			System.out.println("***********************************");
   		System.out.println("*          PARKED VEHICLES        * \n*=================================*\n* Type                  Number    *");
      System.out.println("***********************************");
		}
}

class parkingManagement{
	public static void main(String[] argv){
		DataInputStream userChoice=new DataInputStream(System.in);
		userInterface SystemUI=new userInterface();
		vehicle vehicleObject=new vehicle();
		SystemUI.greet();
		String[] args={};
		int choice;
		try{
			choice=Integer.parseInt(userChoice.readLine());
			switch(choice){
				case 1:
						vehicleObject.getData();
						break;
				case 2:
						vehicleObject.removeData();
						main(args);
						break;
				case 3:
						SystemUI.table();
						vehicleObject.display();
						main(args);
						break;
				case 4:
						System.out.println("\n:)\n");
						System.exit(0);
				default:
						System.out.println("Please enter a valid choice! ");
			}
		}catch(Exception e){}
	}
}

